import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import SideBar from './sidebar.component';
import { Platform } from 'react-native';

import HomeModule from '../modules/home/home.component';
import SettingModule from '../modules/setting/setting.component';
import ModelModule from '../modules/model';

const AuthorizeNavigator = createStackNavigator({
    Home: {
        screen: HomeModule
    },
    Setting: {
        screen: SettingModule
    },
    // mỗi module tương ứng với 1 route dưới đây
    Model: {
        screen: ModelModule
    }
}, {
    headerMode: 'none',
    initialRouteName: 'Home'
});

export const AppDrawerNavigator = createDrawerNavigator({
    Home: AuthorizeNavigator
}, {
    contentComponent: props => <SideBar {...props} />
});