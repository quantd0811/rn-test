import React from 'react';
import { SafeAreaView, Text, View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { Header, Left, Right, Button, Body, Title, List, ListItem, Icon, Content } from 'native-base';

export const HomeTemplate = function () {
    return (
        <Content style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header>
                <Left>
                    <Button transparent onPress={() => { this.props.navigation.openDrawer() }}>
                        <Icon name='menu' type="MaterialCommunityIcons" style={styles.headerIcon} />
                    </Button>
                </Left>
                <Body>
                    <Title>Header</Title>
                </Body>
                <Right></Right>
            </Header>
        </Content>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between'
    },
    bottomToolsContainer: {
        backgroundColor: '#3f51b5',
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    bottomTool: {
        alignItems: 'center'
    },
    headerIcon: {
        fontSize: 24,
        color: '#fff'
    }
})