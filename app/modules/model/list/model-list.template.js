import React from 'react';
import { Container, Header, Left, Button, Icon, Body, Title, Right, Text, Content, List, ListItem } from 'native-base';
import LoadingOverlay from '../../../shared/components/loading-overlay';
import { RefreshControl } from 'react-native';

export default ModelListTemplate = function () {
    return (
        <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name="chevron-left" type="MaterialCommunityIcons"></Icon>
                    </Button>
                </Left>
                <Body>
                    <Title>HEADER_TEXT</Title>
                </Body>
                <Right>
                    <Button transparent onPress={this.onAdd.bind(this)}>
                        <Text>Thêm</Text>
                    </Button>
                </Right>
            </Header>
            <Content>
                <List
                    refreshControl={<RefreshControl refreshing={this.props.fetchingList}
                        onRefresh={this.onRefresh.bind(this)}>
                    </RefreshControl>}
                    dataArray={this.props.models}
                    renderItem={data => (
                        <ListItem onPress={this.onDetail.bind(this, data.item.id)}>
                            <Left>
                                {/* tuy vao thong tin can hien thi */}
                                <Text>{data.item.name}</Text>
                            </Left>
                            <Right>
                                <Button onPress={this.onDelete.bind(this, data.item.id)}>
                                    <Icon name="trash-can" type="MaterialCommunityIcons"></Icon>
                                </Button>
                            </Right>
                        </ListItem>
                    )}
                    onEndReachedThreshold={0.5}
                    onEndReached={this.loadMore.bind(this)}
                />
            </Content>
        </Container>
    )
}