import React from 'react';
import { connect } from 'react-redux';
import ModelListTemplate from './model-list.template';
import { getAll, getSingle, resetModel, deleteModel } from '../../../redux/model/model.action';
import { Danger, Success } from '../../../shared/utils/custom-alert';

class ModelList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            pageSize: 10,
            filter: ''
        }
    }

    componentWillReceiveProps(props) {
        this.showResultMessage(this.props, props);
    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        this.props.getAll(this.state.page, this.stage.pageSize, this.state.filter);
    }

    loadMore() {
        if (!this.props.hasMore) return;
        this.setState({
            page: this.state.page + 1
        }, () => this.loadData())
    }

    onRefresh() {
        this.setState({
            page: 1
        }, () => {
            this.loadData();
        })
    }

    onAdd() {
        this.props.resetModel();
        this.props.navigation.navigate('Create');
    }

    onDelete(id) {
        this.props.deleteModel(id);
    }

    onDetail(id) {
        this.props.getSingle(id);
        this.props.navigation.navigate('Create');
    }

    showResultMessage(prevProps, currentProps) {
        // hiển thị thông báo xóa
        if (prevProps.deleting && !currentProps.deleting) {
            if (currentProps.error)
                Danger(currentProps.error);
            else
                Success('Xóa thành công');
        }
        if (prevProps.fetchingList && !currentProps.fetchingList) {
            if (currentProps.error)
                Danger(currentProps.error);
        }
    }

    render() {
        return ModelListTemplate.call(this);
    }
}

const mapStateToProps = state => {
    return {
        models: state.modelReducer.models,
        fetchingList: state.modelReducer.fetchingList,
        error: state.modelReducer.error,
        hasMore: state.modelReducer.hasMore,
        deleting: state.modelReducer.deleting
    }
}

export default connect(mapStateToProps, { getAll, getSingle, resetModel, deleteModel })(ModelList);