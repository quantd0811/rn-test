import ModelCreateComponent from './create/model-create.component';
import ModelListComponent from './list/model-list.component';

export default createStackNavigator({
    List: {
        screen: ModelListComponent
    },
    Create: {
        screen: ModelCreateComponent
    }
}, {
    headerMode: 'none',
    initialRouteName: 'List'
})