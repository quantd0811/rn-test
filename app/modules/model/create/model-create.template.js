import React from 'react';
import { Container, Header, Icon, Left, Right, Content, Button } from 'native-base';
import LoadingOverlay from '../../../shared/components/loading-overlay';

export default ModelCreateTemplate = function () {
    return (
        <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name="chevron-left" type="MaterialCommunityIcons"></Icon>
                    </Button>
                </Left>
                <Body>
                    <Title>HEADER_TEXT</Title>
                </Body>
                <Right>
                    <Button transparent onPress={this.onSave.bind(this)}>
                        <Text>Lưu</Text>
                    </Button>
                </Right>
            </Header>
            <Content>
                {/* giao diện form ở đây */}
                {/* hiển thị loading icon khi lưu */}
                {this.props.saving ? <LoadingOverlay /> : null} 
            </Content>
        </Container>
    )
}