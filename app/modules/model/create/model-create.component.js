import React from 'react';
import { connect } from 'react-redux';
import ModelCreateTemplate from './model-create.template';
import { save } from '../../../redux/model/model.action';
import { Success, Danger } from '../../../shared/utils/custom-alert';

class ModelCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model: {}
        }
    }

    componentWillReceiveProps(props) {
        this.showResultMessage(this.props, props); // hiển thị message, thành công hoặc báo lỗi

        this.setState({
            model: props.model
        })
    }

    showResultMessage(prevProps, currentProps) {
        if (prevProps.saving && !currentProps.saving) {
            if (currentProps.error)
                Danger(currentProps.error);
            else
                Success('Lưu thành công');
        }
    }

    onSave() {
        this.props.save(this.state.model); // create hoặc update, create nếu id=0, update nếu id!=0
    }

    render() {
        return ModelCreateTemplate.call(this);
    }
}

const mapStateToProps = state => {
    return {
        model: state.modelReducer.model,
        saving: state.modelReducer.saving,
        error: state.modelReducer.error
    }
}

export default connect(mapStateToProps, { save })(ModelCreate);