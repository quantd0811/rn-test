import { LOGIN, LOGOUT, LOGIN_FAIL, LOGIN_SUCCESS, CLEAR_ERROR } from './login.constant';

export function login(username, password) {
    return {
        type: LOGIN,
        username: username,
        password: password
    }
}


export function logout() {
    return {
        type: LOGOUT
    }
}

export function loginSuccess(data) {
    return {
        type: LOGIN_SUCCESS,
        token: data.token
    }
}

export function loginFail(error) {
    return {
        type: LOGIN_FAIL,
        error: error
    }
}

export function clearError() {
    return {
        type: CLEAR_ERROR
    }
}