import { combineReducers } from 'redux';
import loginReducer from './login/login.reducer';
import modelReducer from './model/model.reducer';

const rootReducer = combineReducers({   
    loginReducer,
    modelReducer
})

export default rootReducer;