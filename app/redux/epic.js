import { combineEpics } from 'redux-observable';
import { loginEpic } from './login/login.epic';
import * as modelEpic from './model/model.epic';

export default epics = combineEpics(
    loginEpic,
    modelEpic.saveEpic,
    modelEpic.getSingleEpic,
    modelEpic.getAllEpic,
    modelEpic.deleteEpic
)