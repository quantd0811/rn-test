import * as constants from './model.constant';
import * as actions from './model.action';
import { API_URL } from '../../core/app.setting';
import { Http } from '../../shared/libs/http';
import { mergeMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';

// epics sử dụng để call api, các action tương tác với server cần sử dụng epic

export const saveEpic = action$ => action$.pipe(
    ofType(constants.MODEL_SAVE),
    mergeMap(action => {
        return Http.post(API_URL + '/model/', action.model)
            .pipe(
                map(model => actions.saveSuccess(model)),
                catchError(err => of(actions.saveFail(err)))
            )
    })
);

export const getAllEpic = action$ => action$.pipe(
    ofType(constants.MODEL_GET_ALL),
    mergeMap(action => {
        return Http.get(API_URL + '/model/?page=' + action.page + '&pageSize=' + action.pageSize)
            .pipe(
                map(data => actions.getAllSuccess(data.models, data.total)),
                catchError(err => of(actions.getAllFail(err)))
            )
    })
);


export const getSingleEpic = action$ => action$.pipe(
    ofType(constants.MODEL_GET_SINGLE),
    mergeMap(action => {
        return Http.get(API_URL + '/model/' + action.id)
            .pipe(
                map(model => actions.getSingleSuccess(model)),
                catchError(err => of(actions.getSingleFail(err)))
            )
    })
);

export const deleteEpic = action$ => action$.pipe(
    ofType(constants.MODEL_DELETE),
    mergeMap(action => {
        return Http.delete(API_URL + '/model/' + action.id)
            .pipe(
                map(response => actions.deleteModelSuccess()),
                catchError(err => of(actions.deleteModelFail(err)))
            )
    })
);