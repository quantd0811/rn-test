import * as constants from './model.constant';

const modelState = {
    model: {}, // get by id model
    models: [], // get all models
    total: 0, // tổng số bản ghi của models
    error: null, // error message
    fetchingAll: false, // == true khi đang get all
    fetchingSingle: false, // == true khi đang ge by id
    saving: false, // == true khi đang lưu
    deleting: false, // == true khi đang delete,
    saved: null, // bản ghi vừa save,
    hasMore: true, // == true nếu còn dữ liệu page tiếp theo
}

export default modelReducer = (state = modelState, action) => {
    switch (action.type) {
        case constants.MODEL_SAVE:
            return Object.assign({}, state, {
                saving: true
            })
        case constants.MODEL_SAVE_SUCCESS:
            return Object.assign({}, state, {
                saving: false,
                error: null,
                saved: action.model
            })
        case constants.MODEL_SAVE_FAIL:
            return Object.assign({}, state, {
                saving: false,
                error: action.error
            })
        case constants.MODEL_GET_ALL:
            return Object.assign({}, state, {
                fetchingAll: true,
                hasMore: true
            })
        case constants.MODEL_GET_ALL_SUCCESS:
            return Object.assign({}, state, {
                fetchingAll: false,
                error: null,
                models: [...state.models, action.models],
                total: action.total,
                hasMore: action.models.length > 0
            })
        case constants.MODEL_GET_ALL_FAIL:
            return Object.assign({}, state, {
                fetchingAll: false,
                error: action.error
            })
        case constants.MODEL_GET_SINGLE:
            return Object.assign({}, state, {
                fetchingSingle: true
            })
        case constants.MODEL_GET_SINGLE_SUCCESS:
            return Object.assign({}, state, {
                fetchingSingle: false,
                error: null,
                model: action.model
            })
        case constants.MODEL_GET_SINGLE_FAIL:
            return Object.assign({}, state, {
                fetchingSingle: false,
                error: action.error
            })
        case constants.MODEL_DELETE:
            return Object.assign({}, state, {
                deleting: true
            })
        case constants.MODEL_DELETE_SUCCESS:
            return Object.assign({}, state, {
                deleting: false,
                error: null
            })
        case constants.MODEL_DELETE_FAIL:
            return Object.assign({}, state, {
                deleting: false,
                error: action.error
            })
        case constants.MODEL_RESET_SINGLE:
            return Object.assign({}, state, {
                model: {}
            })
        default: return state;
    }
}
