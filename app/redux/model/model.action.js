import * as constant from './model.constant';

// create or update model
export function save(model) {
    return {
        type: constant.MODEL_SAVE,
        model: model
    }
}
export function saveSuccess(model) {
    return {
        type: constant.MODEL_SAVE_SUCCESS,
        model: model
    }
}
export function saveFail(error) {
    return {
        type: constant.MODEL_SAVE_FAIL,
        error: error
    }
}

// get all with paging and filter
export function getAll(page, pageSize, filter) {
    return {
        type: constant.MODEL_GET_ALL,
        page: page,
        pageSize: pageSize,
        filter: filter
    }
}
export function getAllSuccess(models, total) {
    return {
        type: constant.MODEL_GET_ALL_SUCCESS,
        models: models,
        total: total
    }
}
export function getAllFail(error) {
    return {
        type: constant.MODEL_GET_ALL_FAIL,
        error: error
    }
}

// get model by id
export function getSingle(id) {
    return {
        type: constant.MODEL_GET_SINGLE,
        id: id
    }
}
export function getSingleSuccess(model) {
    return {
        type: constant.MODEL_GET_SINGLE_SUCCESS,
        model: model
    }
}
export function getSingleFail(error) {
    return {
        type: constant.MODEL_GET_SINGLE_FAIL,
        error: error
    }
}

// delete by id
export function deleteModel(id) {
    return {
        type: constant.MODEL_DELETE,
    }
}
export function deleteModelFail(error) {
    return {
        type: constant.MODEL_DELETE_FAIL,
        error: error
    }
}
export function deleteModelSuccess() {
    return {
        type: constant.MODEL_DELETE_SUCCESS
    }
}

export function resetModel() {
    return {
        type: constant.MODEL_RESET_SINGLE
    }
}