import React from 'react';
import Modal from 'react-native-modal';
import { View, Text } from 'react-native';
import LoadingIcon from './loading-icon';

export default class LoadingOverlay extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal
                animationIn="fadeIn"
                animationOut="fadeOut"
                animationInTiming={100}
                animationOutTiming={100}
                isVisible={true}
                backdropOpacity={0.6}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <LoadingIcon />
                    <Text style={{ color: '#e6e6e6' }}>Đang xử lý, vui lòng chờ</Text>
                </View>
            </Modal>
        )
    }
}