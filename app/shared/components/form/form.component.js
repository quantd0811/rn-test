import React from 'react';
import { View, StyleSheet } from 'react-native';
import TextField from './text-field.component';
import CheckboxField from './checkbox-field.component';
import DropDownArray from './dropdown-array.component';
import { Icon, ListItem, Item } from 'native-base';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.controls = [];
        this.state = {
            fields: this.convertPropertiesToArray(props.model),
            errors: []
        }
    }

    convertPropertiesToArray(model) {
        let properties = [];
        for (key in model) {
            properties.push(model[key]);
        }
        return properties;
    }

    valid() {
        debugger
        return this.controls.filter(control => !control.ref.valid()).length == 0;
    }

    renderField(field) {
        switch (field.type) {
            case 'number':
            case 'dropdown':
                return <DropDownArray
                    title={field.name}
                    renderIcon={() => (
                        <Icon name="angle-right" type="FontAwesome"></Icon>
                    )}
                    items={field.items}
                    value={field.value}
                    onChange={value => this.updateFieldValue(field.name, value)} />
            case 'boolean':
                return <CheckboxField ref={ref => this.addControl({ name: field.name, ref: ref })} validators={field.validators}
                    name={field.name}
                    value={field.value}
                    onValueChange={value => this.updateFieldValue(field.name, value)} />;
            case 'text':
                return <TextField ref={ref => this.addControl({ name: field.name, ref: ref })} validators={field.validators}
                    name={field.name}
                    value={field.value}
                    onValueChange={value => this.updateFieldValue(field.name, value)} />;
        }
    }

    addControl(newControl) {
        if (this.controls.filter(control => control.name == newControl.name).length > 0)
            return;
        this.controls.push(newControl);
    }

    updateFieldValue(fieldName, newValue) {
        this.setState({
            fields: this.state.fields.map(field => {
                if (field.name == fieldName) {
                    return Object.assign({}, field, {
                        value: newValue
                    })
                }
                return field;
            })
        })
    }

    render() {
        return (
            <View style={styles.form}>
                {this.state.fields.map(field => this.renderField(field))}
            </View>
        )
    }
}

export default Form;

const styles = StyleSheet.create({
    form: {

    }
})